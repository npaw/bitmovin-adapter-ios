//
//  ViewModelObjc.h
//  BasicAds
//
//  Created by Tiago Pereira on 25/03/2020.
//  Copyright © 2020 Bitmovin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BitmovinPlayer/BitmovinPlayer.h>

@import YouboraLib;

@protocol ViewModel <NSObject>

-(void)initPlugin:(YBOptions*)options;
-(void)setAdapter:(BMPBitmovinPlayer*)player;
-(void)setAdsAdapter:(BMPBitmovinPlayer*)player;
-(void)stopPlayer;


@end

@interface ViewModelObjc : NSObject <ViewModel>

@end
