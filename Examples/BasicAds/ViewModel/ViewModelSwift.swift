//
//  ViewModelSwift.swift
//  BasicAds
//
//  Created by Tiago Pereira on 25/03/2020.
//  Copyright © 2020 Bitmovin. All rights reserved.
//

import Foundation
import YouboraLib
import BitmovinPlayer
import YouboraBitmovinAdapter

class ViewModelSwift:NSObject, ViewModel {
    
    var plugin: YBPlugin?
    
    func initPlugin(_ options: YBOptions) {
        self.plugin = YBPlugin(options: options)
    }
    
    func setAdapter(_ player: Player) {
        self.plugin?.adapter = YBBitmovinAdapterSwiftTranformer.transform(from: YBBitmovinAdapter(player: player))
        
    }
    
    func setAdsAdapter(_ player: Player) {
        self.plugin?.adsAdapter = YBBitmovinAdapterSwiftTranformer.transform(fromAdsAdapter: YBBitmovinAdAdapter(player: player))
    }
    
    func stopPlayer() {
        self.plugin?.fireStop()
        self.plugin?.removeAdapter()
        self.plugin?.removeAdsAdapter()
    }
}
