//
//  ViewModelObjc.m
//  BasicAds
//
//  Created by Tiago Pereira on 25/03/2020.
//  Copyright © 2020 Bitmovin. All rights reserved.
//

#import "ViewModelObjc.h"
#import <YouboraBitmovinAdapter/YouboraBitmovinAdapter.h>

@interface ViewModelObjc ()

@property YBPlugin *plugin;

@end

@implementation ViewModelObjc

- (void)initPlugin:(YBOptions *)options {
    self.plugin = [[YBPlugin alloc] initWithOptions:options];
}

- (void)setAdapter:(BMPBitmovinPlayer *)player {
    [self.plugin setAdapter: [[YBBitmovinAdapter alloc] initWithPlayer:player]];
}

- (void)setAdsAdapter:(BMPBitmovinPlayer *)player {
    [self.plugin setAdsAdapter: [[YBBitmovinAdAdapter alloc] initWithPlayer:player]];
}

- (void)stopPlayer {
    [self.plugin fireStop];
    [self.plugin removeAdapter];
    [self.plugin removeAdsAdapter];
}

@end
