//
// Bitmovin Player iOS SDK
// Copyright (C) 2017, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

import UIKit
import BitmovinPlayer
import YouboraBitmovinAdapter
import YouboraConfigUtils

final class ViewController: UIViewController {
    public var adTagVastSkippable: String?
    
    public var adTagVast1: String?
    public var adTagVast2: String?
    
    var player: Player?
    
    @IBOutlet weak var playerContainer: UIView!
    
    let viewModel = ViewModelSwift()
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    deinit {
        player?.destroy()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Define needed resources
        guard let streamUrl = URL(string: "https://bitmovin-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8") else {
            return
        }
        
        // Create player configuration
        let config = PlayerConfiguration()
        
        // Create Advertising configuration
        let adSource1 = AdSource(tag: urlWithCorrelator(adTag: adTagVastSkippable ?? ""), ofType: .IMA)
        let adSource2 = AdSource(tag: urlWithCorrelator(adTag: adTagVast1 ?? ""), ofType: .IMA)
        let adSource3 = AdSource(tag: urlWithCorrelator(adTag: adTagVast2 ?? ""), ofType: .IMA)
        
        let preRoll = AdItem(adSources: [adSource1], atPosition: "pre")
        let midRoll = AdItem(adSources: [adSource2], atPosition: "20%")
        let postRoll = AdItem(adSources: [adSource3], atPosition: "post")
        
        let adConfig = AdvertisingConfiguration(schedule: [preRoll, midRoll, postRoll])
        config.advertisingConfiguration = adConfig
        
        do {
            try config.setSourceItem(url: streamUrl)
            
            // Create player based on player configuration
            let player = Player(configuration: config)
            
            YBLog.setDebugLevel(YBLogLevel.verbose)
            
            let options = YouboraConfigManager.getOptions()
            
            options.contentResource = streamUrl.absoluteString
            options.contentIsLive = NSNumber(value: false)
            
            self.viewModel.initPlugin(options)
            
            self.viewModel.setAdapter(player)
            self.viewModel.setAdsAdapter(player)
            
            // Create player view and pass the player instance to it
            let playerView = BMPBitmovinPlayerView(player: player, frame: .zero)

            // Listen to player events
            player.add(listener: self)

            playerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            playerView.frame = view.frame
            
            view.addSubview(playerView)
            view.bringSubview(toFront: playerView)
            self.player = player
        } catch {
            print("Configuration error: \(error)")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .black
    }
    
    
    
    func urlWithCorrelator(adTag: String) -> URL {
        return URL(string: String(format: "%@%d", adTag, Int(arc4random_uniform(100000))))!
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if self.isMovingToParentViewController {
            self.viewModel.stopPlayer()
        }
    }
}

extension ViewController: PlayerListener {
    
    func onAdScheduled(_ event: AdScheduledEvent) {
        print("onAdScheduled \(event.timestamp)")
    }
    
    func onAdBreakStarted(_ event: AdBreakStartedEvent) {
        print("onAdBreakStarted \(event.timestamp)")
    }
    
    func onAdStarted(_ event: AdStartedEvent) {
        print("onAdStarted \(event.timestamp)")
    }
    
    func onAdFinished(_ event: AdFinishedEvent) {
        print("onAdFinished \(event.timestamp)")
    }
    
    func onAdBreakFinished(_ event: AdBreakFinishedEvent) {
        print("onAdBreakFinished \(event.timestamp)")
    }
    
    func onAdError(_ event: AdErrorEvent) {
        print("onAdError \(event.timestamp)")
    }
}
