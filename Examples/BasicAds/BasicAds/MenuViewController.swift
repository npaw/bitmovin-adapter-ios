//
//  MenuViewController.swift
//  BasicAds
//
//  Created by Tiago Pereira on 20/11/2020.
//  Copyright © 2020 Bitmovin. All rights reserved.
//

import UIKit
import YouboraConfigUtils

class MenuViewController: UIViewController {
    
    @IBOutlet weak var ad1TextField: UITextField!
    @IBOutlet weak var ad2TextField: UITextField!
    @IBOutlet weak var ad3TextField: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        ad1TextField.text = "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dskippablelinear&correlator="
        ad2TextField.text = "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dlinear&correlator="
        ad3TextField.text = "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/32573358/2nd_test_ad_unit&ciu_szs=300x100&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&url=[referrer_url]&description_url=[description_url]&correlator="
        

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onSettings(_ sender: UIButton) {
        self.navigationController?.present(YouboraConfigViewController.initFromXIB(), animated: true, completion: nil)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//         Get the new view controller using segue.destination.
//         Pass the selected object to the new view controller.
        
        if let viewController = segue.destination as? ViewController {
            viewController.adTagVastSkippable = ad1TextField.text
            viewController.adTagVast1 = ad2TextField.text
            viewController.adTagVast2 = ad3TextField.text
        }
    }

}
