//
// Bitmovin Player iOS SDK
// Copyright (C) 2017, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

import UIKit
import BitmovinPlayer
import YouboraBitmovinAdapter
import YouboraConfigUtils

final class ViewController: UIViewController, PlayerListener {
    
    var player: Player?

    var plugin:YBPlugin?
    
    @IBOutlet weak var btnSettings: UIButton!
    
    @IBOutlet weak var seekText: UITextField!
    @IBOutlet weak var videoContainer: UIView!
    
    deinit {
        player?.destroy()
    }
    
    @IBAction func onPlayBtn(_ sender: UIButton) {
        player?.play()
    }
    
    @IBAction func onPauseBtn(_ sender: UIButton) {
        player?.pause()
    }
    
    @IBAction func onSeekBtn(_ sender: UIButton) {
        guard let seekText = self.seekText.text,
        let seek = Double(seekText) else {
            return
        }
        
        player?.seek(time: TimeInterval(seek))
    }
    
    @IBAction func btnSettingsTapped(_ sender: UIButton) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        let vc = YouboraConfigViewController.initFromXIB()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let options = YouboraConfigManager.getOptions()
        
        self.plugin = YBPlugin(options: options)
        
        self.plugin?.getInfinity().begin(withScreenName: "test")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.plugin?.getInfinity().fireNav(withScreenName: "test")
        }
        
        self.view.backgroundColor = .black
        
        // Define needed resources
        guard let streamUrl = URL(string: "https://bitmovin-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8"),
            let posterUrl = URL(string: "https://bitmovin-a.akamaihd.net/content/MI201109210084_1/poster.jpg") else {
                return
        }
        
        // Create player configuration
        let config = PlayerConfiguration()
        
        do {
            try config.setSourceItem(url: streamUrl)
            
            // Set a poster image
            config.sourceItem?.posterSource = posterUrl
            
            // Create player based on player configuration
            let player = Player(configuration: config)
            
            // Create player view and pass the player instance to it
            let playerView = BMPBitmovinPlayerView(player: player, frame: .zero)
            
            // Listen to player events
            player.add(listener: self)
                        
            //playerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            playerView.frame = view.bounds
            
            videoContainer.addSubview(playerView)
            videoContainer.bringSubview(toFront: playerView)
            
            btnSettings.superview?.bringSubview(toFront: btnSettings)
            self.player = player
            
            plugin?.fireInit()
            plugin?.adapter = YBBitmovinAdapterSwiftTranformer.transform(from: YBBitmovinAdapter(player: player))
        } catch {
            print("Configuration error: \(error)")
        }
        
        
        YBLog.setDebugLevel(YBLogLevel.verbose)
    }
    
    @IBOutlet weak var currentTime: UILabel!
    
    @IBOutlet weak var loadedText: UILabel!
    
    @IBOutlet weak var duration: UILabel!
        
    func onPlay(_ event: PlayEvent) {
        print("onPlay \(event.time)")
    }
    
    func onPaused(_ event: PausedEvent) {
        print("onPaused \(event.time)")
    }
    
    func onTimeChanged(_ event: TimeChangedEvent) {
        currentTime.text = String(event.currentTime)
        print("onTimeChanged \(event.currentTime)")
    }
    
    func onDurationChanged(_ event: DurationChangedEvent) {
        duration.text = String(event.duration)
        print("onDurationChanged \(event.duration)")
    }
    
    func onError(_ event: ErrorEvent) {
        print("onError \(event.message)")
    }
}
