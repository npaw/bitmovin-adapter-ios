//
//  YBBitmovinAdapterSwiftWrapper.h
//  YouboraBitmovinAdapter
//
//  Created by Enrique Alfonso Burillo on 30/07/2018.
//  Copyright © 2018 Enrique Alfonso Burillo. All rights reserved.
//

#import "YBBitmovinAdapter.h"

__attribute__ ((deprecated)) DEPRECATED_MSG_ATTRIBUTE("This class is deprecated. Use YBBitmovinAdapterSwiftTranformer instead")
@interface YBBitmovinAdapterSwiftWrapper : NSObject

@property(nonatomic,strong) YBPlugin* plugin;
@property(nonatomic,strong) YBBitmovinAdapter* adapter;
@property(nonatomic,strong) NSObject* player;

- (id) initWithPlayer:(NSObject*)player andPlugin:(YBPlugin*)plugin;
- (void) fireStart;
- (void) fireStop;
- (void) firePause;
- (void) fireResume;
- (void) fireJoin;

- (void) removeAdapter;
- (BMPBitmovinPlayer *) getPlayer;

@end

