//
//  YBBitmovinAdapter.h
//  YouboraBitmovinAdapter
//
//  Created by Enrique Alfonso Burillo on 30/07/2018.
//  Copyright © 2018 Enrique Alfonso Burillo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YouboraLib/YouboraLib.h"
#import "BitmovinPlayer/BitmovinPlayer.h"

@interface YBBitmovinAdAdapter : YBPlayerAdapter<BMPBitmovinPlayer *> <BMPPlayerListener>

@end
