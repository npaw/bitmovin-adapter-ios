//
//  YBAVPlayerAdapterSwiftTranformer.h
//  YouboraBitmovinAdapter
//
//  Created by nice on 03/01/2020.
//  Copyright © 2020 Enrique Alfonso Burillo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YBBitmovinAdapter.h"
#import "YBBitmovinAdAdapter.h"

@interface YBBitmovinAdapterSwiftTranformer : NSObject

+(YBPlayerAdapter<id>*)transformFromAdapter:(YBBitmovinAdapter*)adapter;
+(YBPlayerAdapter<id>*)transformFromAdsAdapter:(YBBitmovinAdAdapter*)adapter;
@end
