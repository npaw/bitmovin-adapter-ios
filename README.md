# YouboraBitmovinAdapter

A framework that will collect several video events from the BitmovinPlayer and send it to the back end

# Installation

#### Carthage

Create your Cartfile and insert 

```bash
git "https://bitbucket.org/npaw/bitmovin-adapter-ios.git"
```

and then run

```bash
carthage update
```

when the update finishes go to **{YOUR_SCHEME} > Build Settings > Framework Search Paths** and add **\$(PROJECT_DIR)/Carthage/Build/{iOS, Mac, tvOS or the platform of your scheme}**

#### CocoaPods

Insert into your Podfile

add this sources to your Podfile

```bash
source 'https://bitbucket.org/npaw/ios-sdk-podspecs.git'
source 'https://github.com/bitmovin/cocoapod-specs.git'
source 'https://github.com/CocoaPods/Specs.git'
```

add this pod to your target

```bash
pod 'YouboraBitmovinAdapter'
```

and then run

```bash
pod install
```

## How to use

## Start plugin and options

#### Swift

```swift

//Import
import YouboraLib

...

//Config Options and init plugin (do it just once for each play session)

var options: YBOptions {
        let options = YBOptions()
        options.contentResource = "http://example.com"
        options.accountCode = "accountCode"
        options.adResource = "http://example.com"
        options.contentIsLive = NSNumber(value: false)
        return options;
    }
    
lazy var plugin = YBPlugin(options: self.options)
```

### YBBitmovinAdapter & YBBitmovinAdAdapter

#### Swift

```swift
import YouboraBitmovinAdapter
...

//Once you have your player and plugin initialized you can set the adapter
plugin.adapter = YBBitmovinAdapterSwiftTranformer.transform(from: YBBitmovinAdapter(player: player))

...

//If you want to setup the ads adapter
plugin.adsAdapter = YBBitmovinAdapterSwiftTranformer.transform(fromAdsAdapter: YBBitmovinAdAdapter(player: player))
```

## Run samples project

###### Via Carthage (Default)

Open terminal in your root folder and execute: 

```bash
carthage update && cd Example && carthage update
```

Navigate to the root folder and then execute: 


###### Via Cocoapods

```bash
pod install
```

---
**NOTES**

Case you have problems with Swift headers not found, when you're trying to install via **carthage** please do the follow instructions: 

 1. Remove Carthage folder from the project
 2. Execute the follow command ```rm -rf ~/Library/Caches/org.carthage.CarthageKit```
 3. This project should add cocoapods folder to the ignore file otherwise carthage will stop working

---