## [6.6.0] - 2021-10-11
### Added 
- Support to Xcode 13.x

## [6.5.11] - 2021-07-08
### Fixed 
- Micro seeks to one unique seek

## [6.5.10] - 2021-04-08
### Changed 
- Support to BitmovinPlayer v2.x

## [6.5.9] - 2021-03-31
### Fixed 
- Adapter version detected on logs

## [6.5.8] - 2021-02-01
### Fixed 
- Deleted pods to fix carhage use in Xcode inferior to 12

## [6.5.7] - 2021-01-07
### Changed 
- Support to BitmovinPlayer => v.2.58.x

## [6.5.6] - 2020-11-25
### Added 
- Ad duration to the ads adapter

## [6.5.5] - 2020-11-23
### Changed
- Support to BitmovinPlayer => v.2.53.x

### Removed
- Auto initialization of the ads adapter

## [6.5.4] - 2020-03-17
### Added
- Support to Carthage

## [6.5.3] - 2020-03-04
### Added
- Support to cocoapods

### Changed
- Support to BitmovinPlayer v.2.41.0

## [6.5.2] - 2020-02-13
### Fixed
- Buffers

## [6.5.1] - 2019-11-12
### Added
- New ads
- tvOS support

## [6.5.0] - 2019-07-24
### Fixed
- Problem with buffer when seeking

## [6.0.0] - 2018-09-27
### Added
- First release
