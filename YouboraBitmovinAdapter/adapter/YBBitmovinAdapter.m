//
//  YBBitmovinAdapter.m
//  YouboraBitmovinAdapter
//
//  Created by Enrique Alfonso Burillo on 30/07/2018.
//  Copyright © 2018 Enrique Alfonso Burillo. All rights reserved.
//

#import "YBBitmovinAdapter.h"
#import "YBBitmovinAdAdapter.h"
#import <TargetConditionals.h>

// Constants
#define MACRO_NAME(f) #f
#define MACRO_VALUE(f) MACRO_NAME(f)

#define PLUGIN_VERSION_DEF MACRO_VALUE(YOUBORABITMOVINADAPTER_VERSION)
#define PLUGIN_NAME_DEF "Bitmovin"

#if TARGET_OS_TV==1
#define PLUGIN_PLATFORM_DEF "tvOS"
#else
#define PLUGIN_PLATFORM_DEF "iOS"
#endif

#define PLUGIN_NAME @PLUGIN_NAME_DEF "-" PLUGIN_PLATFORM_DEF
#define PLUGIN_VERSION @PLUGIN_VERSION_DEF "-" PLUGIN_NAME_DEF "-" PLUGIN_PLATFORM_DEF

@interface YBBitmovinAdapter()

@property (nonatomic,strong) NSNumber * lastReportedBitrate;

@property (nonatomic,strong) YBTimer * joinTimer;

@property (nonatomic, assign) NSTimeInterval lastSeekTimeStamp;

@end

@implementation YBBitmovinAdapter

- (void) registerListeners {
    [super registerListeners];
    
    __weak typeof(self) weakSelf = self;
    self.joinTimer = [[YBTimer alloc] initWithCallback:^(YBTimer * timer, long long diffTime){
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            if ([self getPlayhead] != nil && [[self getPlayhead] doubleValue] > 0.1 && self.flags.started) {
                [strongSelf fireJoin];
                [strongSelf.joinTimer stop];
            }
        }
    } andInterval:100];
    
    self.lastReportedBitrate = @(-1);
    [self.player addPlayerListener:self];
    
    [self monitorPlayheadWithBuffers:false seeks:true andInterval:800];
}

- (void) unregisterListeners {
    [self.player removePlayerListener:self];
    [self.monitor stop];
}

- (NSNumber *) getPlayhead {
    return self.player.currentTime < 0.001 ? @(0) : @(self.player.currentTime);
}

- (NSNumber *) getPlayrate {
    if (self.flags.paused) {
        return [super getPlayrate];
    }
    return @(self.player.playbackSpeed);
}

- (NSNumber *) getDuration {
    double duration = self.player.duration;
    if (duration > 0) {
        return @(duration);
    } else {
        return [super getDuration];
    }
}

- (NSNumber *) getBitrate {
    if (self.flags.paused)
        return self.lastReportedBitrate;
    
    NSUInteger videoBitrateAvg = 0;

    if (self.player.videoQuality != nil)
        videoBitrateAvg = self.player.videoQuality.bitrate;
    
    if (videoBitrateAvg > 0) {
        return self.lastReportedBitrate = @(videoBitrateAvg);
    }
    
    return [super getBitrate];
}

- (NSString *) getRendition {
    BMPVideoQuality *videoQuality = self.player.videoQuality;
    
    if (videoQuality != nil && [self getBitrate] != nil) {
        return [YBYouboraUtils buildRenditionStringWithWidth:(int)videoQuality.width height:(int)videoQuality.height andBitrate:[[self getBitrate] doubleValue]];
    }
    return [super getRendition];
}

- (NSValue *) getIsLive {
    if ([self getDuration] != nil && [[self getDuration] doubleValue] == INFINITY) {
        return @YES;
    } else if ([self getDuration] != nil && [[self getDuration] doubleValue] > 0) {
        return @NO;
    }
    return [super getIsLive];
}

- (NSString *) getResource {
    BMPSourceItem *sourceItem = nil;
    
    if (self.player != nil && self.player.config != nil) {
        sourceItem = self.player.config.sourceItem;
        
        if (sourceItem.dashSource != nil) {
            return [sourceItem.dashSource.url absoluteString];
        } else if (sourceItem.hlsSource != nil) {
            return [sourceItem.hlsSource.url absoluteString];
        }
        return [super getResource];
    }
    return [super getResource];
}

- (NSString *)getPlayerName {
    return PLUGIN_NAME;
}

- (NSString *)getPlayerVersion {
    return @PLUGIN_NAME_DEF;
}

- (NSString *)getVersion {
    return PLUGIN_VERSION;
}

- (void) fireStart {
    [super fireStart];
    [self.joinTimer start];
}

- (void) onPlay:(BMPPlayEvent *)event {
    [self fireResume];
    [self fireStart];
}

- (void) onStallEnded:(BMPStallEndedEvent *)event {
    [self fireBufferEnd];
}

- (void) onStallStarted:(BMPStallStartedEvent *)event {
    [YBLog debug:@"%lf - %lf", self.lastSeekTimeStamp, event.timestamp];
    //if (self.lastSeekTimeStamp + 100 < event.timestamp) {
        [self fireBufferBegin];
    //}
}

- (void) onPaused:(BMPPausedEvent *)event {
    [self firePause];
}

- (void) onPlaybackFinished:(BMPPlaybackFinishedEvent *)event {
    if (!self.player.isAd) {
        [self fireStop];
    }
}

- (void) onAdBreakStarted:(BMPAdBreakStartedEvent *)event {
    if (self.plugin != nil && self.plugin.adsAdapter != nil
            && [self.plugin.adsAdapter getPosition] == YBAdPositionPost) {
        [self fireStop];
    }
}

- (void) onSeek:(BMPSeekEvent *)event {
    [self fireSeekBegin];
}

- (void) onError:(BMPErrorEvent *)event {
    if (event.message == nil) {
        [self fireFatalErrorWithMessage:@"Unknown error" code:nil andMetadata:nil];
    } else {
        [self fireFatalErrorWithMessage:event.message code:[NSString stringWithFormat:@"%lu", (unsigned long)event.code] andMetadata:nil];
    }
}

- (void) onEvent:(BMPPlayerEvent *)event {
    [YBLog debug:@"onEvent: %@", event.name];
}

@end
