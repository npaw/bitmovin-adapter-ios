//
//  YBBitmovinUtils.swift
//  YouboraBitmovinAdapter
//
//  Created by Tiago Pereira on 30/10/2020.
//  Copyright © 2020 Enrique Alfonso Burillo. All rights reserved.
//

import Foundation
import BitmovinPlayer

@objcMembers public class YBBitmovinUtils: NSObject, PlayerListener {
    public static func getPlayhead(player: Any) -> Any {
        var playhead: Double? = nil
        
        if let player = player as? Player {
            playhead = player.currentTime
        }
        
        return playhead as Any
    }
}

