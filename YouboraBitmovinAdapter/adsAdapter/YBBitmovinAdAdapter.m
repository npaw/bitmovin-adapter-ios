//
//  YBBitmovinAdapter.m
//  YouboraBitmovinAdapter
//
//  Created by Enrique Alfonso Burillo on 30/07/2018.
//  Copyright © 2018 Enrique Alfonso Burillo. All rights reserved.
//

#import "YBBitmovinAdAdapter.h"

// Constants
#define MACRO_NAME(f) #f
#define MACRO_VALUE(f) MACRO_NAME(f)

#define PLUGIN_VERSION_DEF MACRO_VALUE(YOUBORABITMOVINADAPTER_VERSION)
#define PLUGIN_NAME_DEF "Bitmovin-Ads"

#if TARGET_OS_TV==1
#define PLUGIN_PLATFORM_DEF "tvOS"
#else
#define PLUGIN_PLATFORM_DEF "iOS"
#endif

#define PLUGIN_NAME @PLUGIN_NAME_DEF "-" PLUGIN_PLATFORM_DEF
#define PLUGIN_VERSION @PLUGIN_VERSION_DEF "-" PLUGIN_NAME_DEF "-" PLUGIN_PLATFORM_DEF

@interface YBBitmovinAdAdapter()

@property(nonatomic, strong) NSString *adPosition;
@property(nonatomic, strong) NSString *adProvider;
@property(nonatomic, strong) NSNumber *adDuration;

@end

@implementation YBBitmovinAdAdapter

- (void) registerListeners {
    [super registerListeners];
    [self.player addPlayerListener:self];
}

- (void) unregisterListeners {
    [self.player removePlayerListener:self];
    [super unregisterListeners];
}

- (void) fireStart {
    [super fireStart];
    [self fireJoin];
}

- (YBAdPosition) getPosition {
    if (self.adPosition == nil) {
        return YBAdPositionUnknown;
    } else if ([self.adPosition isEqualToString:@"pre"]) {
        return YBAdPositionPre;
    } else if ([self.adPosition isEqualToString:@"post"]) {
        return YBAdPositionPost;
    }
    return YBAdPositionMid;
}

- (NSNumber *)getDuration {
    return self.adDuration;
}

- (NSString *) getAdProvider {
    return self.adProvider;
}

- (NSNumber *) getPlayhead {
    return @(self.player.currentTime);
}

- (void)onAdManifestLoaded:(BMPAdManifestLoadedEvent *)event; {
    [self fireAdManifest:nil];
}

- (void)onAdBreakStarted:(BMPAdBreakStartedEvent *)event {
    [self fireAdBreakStart];
}

- (void) onAdStarted:(BMPAdStartedEvent *)event {
    self.adPosition = event.position;
    self.adProvider = event.clientType == BMPAdSourceTypeIMA ? @"IMA" : @"Unknown";
    self.adDuration = @(event.duration);
    [self fireResume];
    [self fireStart];
}

- (void)onAdQuartile:(BMPAdQuartileEvent *)event {
    switch (event.adQuartile) {
        case BMPAdQuartileFirstQuartile:
            [super fireQuartile:1];
            break;
        case BMPAdQuartileMidpoint:
            [super fireQuartile:2];
            break;
        case BMPAdQuartileThirdQuartile:
            [super fireQuartile:3];
            break;
    }
}

- (void) onAdFinished:(BMPAdFinishedEvent *)event {
    [self fireStop];
}

- (void)onAdBreakFinished:(BMPAdBreakFinishedEvent *)event {
    [self fireAdBreakStop];
}

- (void) onAdSkipped:(BMPAdSkippedEvent *)event {
    [self fireStop: @{ @"skipped" : @"true" }];
}

- (void) onAdClicked:(BMPAdClickedEvent *)event {
    NSString *adUrl = @"";
    if (event.clickThroughUrl != nil) {
        adUrl = event.clickThroughUrl.absoluteString;
    }
    [self fireClick: @{@"adUrl" : adUrl}];
}

- (void) onAdError:(BMPAdErrorEvent *)event {
    if (event.message != nil) {
        [self fireFatalErrorWithMessage:@"Unknown error" code:nil andMetadata:nil];
    }
    [self fireFatalErrorWithMessage:event.message code:[NSString stringWithFormat:@"%lu",(unsigned long)event.code] andMetadata:nil];
}

- (NSString *)getPlayerName {
    return PLUGIN_NAME;
}

- (NSString *)getPlayerVersion {
    return @PLUGIN_NAME_DEF;
}

- (NSString *)getVersion {
    return PLUGIN_VERSION;
}

@end
