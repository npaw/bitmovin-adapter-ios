//
//  YBAVPlayerAdapterSwiftTranformer.m
//  YouboraBitmovinAdapter
//
//  Created by nice on 03/01/2020.
//  Copyright © 2020 Enrique Alfonso Burillo. All rights reserved.
//

#import "YBBitmovinAdapterSwiftTranformer.h"

@implementation YBBitmovinAdapterSwiftTranformer

+(YBPlayerAdapter<id>*)transformFromAdapter:(YBPlayerAdapter*)adapter {
    return adapter;
}

+(YBPlayerAdapter<id>*)transformFromAdsAdapter:(YBBitmovinAdAdapter*)adapter {
    return adapter;
}
@end
