//
//  YouboraBitmovinAdapter.h
//  YouboraBitmovinAdapter
//
//  Created by Enrique Alfonso Burillo on 30/07/2018.
//  Copyright © 2018 Enrique Alfonso Burillo. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for YouboraBitmovinAdapter.
FOUNDATION_EXPORT double YouboraBitmovinAdapterVersionNumber;

//! Project version string for YouboraBitmovinAdapter.
FOUNDATION_EXPORT const unsigned char YouboraBitmovinAdapterVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <YouboraBitmovinAdapter/PublicHeader.h>

#import <YouboraBitmovinAdapter/YBBitmovinAdAdapter.h>
#import <YouboraBitmovinAdapter/YBBitmovinAdapter.h>
#import <YouboraBitmovinAdapter/YBBitmovinAdapterSwiftWrapper.h>
#import <YouboraBitmovinAdapter/YBBitmovinAdapterSwiftTranformer.h>
