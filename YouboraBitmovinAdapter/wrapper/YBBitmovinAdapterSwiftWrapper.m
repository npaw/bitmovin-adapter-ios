//
//  YBBitmovinAdapterSwiftWrapper.m
//  YouboraBitmovinAdapter
//
//  Created by Enrique Alfonso Burillo on 30/07/2018.
//  Copyright © 2018 Enrique Alfonso Burillo. All rights reserved.
//

#import "YBBitmovinAdapterSwiftWrapper.h"

@interface YBBitmovinAdapterSwiftWrapper()

@end

@implementation YBBitmovinAdapterSwiftWrapper

- (id) initWithPlayer:(NSObject*)player andPlugin:(YBPlugin*)plugin{
    if (self = [super init]) {
        self.player = player;
        self.plugin = plugin;
    }
    [self initAdapterIfNecessary];
    return self;
}

- (void) fireStart{
    [self initAdapterIfNecessary];
    [self.plugin.adapter fireStart];
}

- (void) fireStop{
    if(self.plugin != nil){
        if(self.plugin.adapter != nil){
            [self initAdapterIfNecessary];
            [self.plugin.adapter fireStop];
            [self.plugin removeAdapter];
        }
    }
    
}
- (void) firePause{
    [self initAdapterIfNecessary];
    [self.plugin.adapter firePause];
}
- (void) fireResume{
    [self initAdapterIfNecessary];
    [self.plugin.adapter fireResume];
}
- (void) fireJoin{
    [self initAdapterIfNecessary];
    [self.plugin.adapter fireJoin];
}

- (void) initAdapterIfNecessary{
    if(self.plugin.adapter == nil){
        if(self.plugin != nil){
            BMPBitmovinPlayer* player = (BMPBitmovinPlayer*) self.player;
            [self.plugin setAdapter:[[YBBitmovinAdapter alloc] initWithPlayer:player]];
            self.adapter = (YBBitmovinAdapter *)self.plugin.adapter;
        }
    }
}

- (void) removeAdapter{
    [self.plugin removeAdapter];
}

- (BMPBitmovinPlayer *) getPlayer {
    return (BMPBitmovinPlayer*) self.player;
}

@end
